import { defineConfig, presetUno, presetWebFonts, transformerDirectives } from "unocss";

export default defineConfig({
	presets: [
		presetUno(),
		presetWebFonts({
			fonts: {
				"sans": { name: "Merriweather", weights: [400, 700] },
			},
		}),
	],
	transformers: [
		transformerDirectives(),
	],
});
